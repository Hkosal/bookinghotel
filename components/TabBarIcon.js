import React from 'react';
import Icon from 'react-native-vector-icons/FontAwesome5';

const TabBarIcon = props => {
  const {focused, size, name} = props;
  return (
    <Icon
      style={{fontSize: size, color: focused ? '#ff6200' : '#05375a'}}
      name={name}
    />
  );
};

export default TabBarIcon;
