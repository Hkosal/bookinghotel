/* eslint-disable no-undef */
import {StyleSheet, View, Text} from 'react-native';

//Navigation

import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {
  createDrawerNavigator,
  DrawerContentScrollView,
  DrawerItemList,
  DrawerItem,
} from '@react-navigation/drawer';
import TabBarIcon from './components/TabBarIcon';
import Feather from 'react-native-vector-icons/Feather';
//Import Files
import Login from './screens/login';
import Post from './screens/postDetails';
import PlaceDetail from './screens/placeDetails';
import HotelDetails from './screens/hotelDetails';
import Verify from './screens/verify';
import Home from './screens/home';
import YourProfile from './screens/yourProfile';
import Category from './screens/categories';
import FavoritePlaces from './screens/favoritePlaces';
import FavoriteHotels from './screens/favoriteHotels';
import Booking from './screens/booking';
import Places from './screens/places';
import Hotels from './screens/hotels';
import Notification from './screens/notification';
import Search from './screens/search';
import AboutUs from './screens/aboutUs';
import HotelsPlacesByProvince from './screens/hotelsPlaceByProvince';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const BottomNavigation = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Home"
        component={Home}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Post"
        component={Post}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="HotelDetails"
        component={HotelDetails}
        options={{headerShown: false}}
      />
    </Stack.Navigator>
  );
};

const NavigationComponent = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen
        name="Home"
        component={BottomNavigation}
        options={{
          tabBarIcon: props => <TabBarIcon {...props} name="house-user" />,
        }}
      />
      <Tab.Screen
        name="Booking"
        component={Booking}
        options={{
          tabBarIcon: props => <TabBarIcon {...props} name="shopping-cart" />,
        }}
      />
      <Tab.Screen
        name="Category"
        component={Category}
        options={{
          tabBarIcon: props => <TabBarIcon {...props} name="bullhorn" />,
        }}
      />
    </Tab.Navigator>
  );
};

const NavigationDrawer = () => {
  return (
    <Drawer.Navigator drawerContent={CustomDrawerContent}>
      <Drawer.Screen component={NavigationComponent} name="Home" />
      <Drawer.Screen component={YourProfile} name="Your Profile" />
      <Drawer.Screen component={FavoritePlaces} name="Favorite Places" />
      <Drawer.Screen component={FavoriteHotels} name="Favorite Hotels" />
      <Drawer.Screen component={Places} name="Places" />
      <Drawer.Screen component={Hotels} name="Hotels" />
      <Drawer.Screen component={Notification} name="Notification" />
      <Drawer.Screen component={AboutUs} name="About Us" />
    </Drawer.Navigator>
  );
};

function CustomDrawerContent(props) {
  return (
    <DrawerContentScrollView {...props}>
      <View>
        <View style={styles.BookTicketBtn}>
          <Text style={styles.bookTicketText}>Booking</Text>
          <Text style={styles.bookTicketText}>App</Text>
        </View>
        <View style={{top: 10, alignSelf: 'center'}}>
          <Text style={{textAlign: 'center'}}>Kong Sreya</Text>
          <Text style={{textAlign: 'center'}}>+855 12 345 678</Text>
        </View>
      </View>
      <View style={{top: 20}}>
        <DrawerItemList {...props} />
      </View>
      <View
        style={{
          marginTop: 50,
          marginLeft: 30,
          marginRight: 100,
          backgroundColor: '#ff6200',
        }}>
        <DrawerItem
          label="Logout"
          onPress={() => {
            props.navigation.navigate('Login');
          }}
        />
      </View>
    </DrawerContentScrollView>
  );
}

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        initialRoute="Login"
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name="Login" component={Login} />
        <Stack.Screen name="verify" component={Verify} />
        <Stack.Screen name="NavigationDrawer" component={NavigationDrawer} />
        <Stack.Screen
          name="HotelsPlacesByProvince"
          component={HotelsPlacesByProvince}
        />
        <Stack.Screen name="PlaceDetail" component={PlaceDetail} />
        <Stack.Screen name="Search" component={Search} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  BookTicketBtn: {
    top: 5,
    height: 110,
    width: 110,
    backgroundColor: '#ff6200',
    padding: 16,
    borderRadius: 100,
    elevation: 5,
    alignSelf: 'center',
  },
  bookTicketText: {
    top: 16,
    alignSelf: 'center',
    color: 'white',
    fontSize: 20,
  },
});
