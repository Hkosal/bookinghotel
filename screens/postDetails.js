/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import config from '../config';
import {useRoute} from '@react-navigation/native';

import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';

const Post = ({navigation}) => {
  const {params = []} = useRoute(); // get from hook

  useEffect(() => {
    getPlace();
    getPlaces();
    getHotels();
  }, []);

  const [place, setPlace] = useState();
  const [places, setPlaces] = useState();
  const [hotels, setHotels] = useState();

  // get place
  async function getPlace() {
    const url = config.API_URL + 'v1/places/place/' + params;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setPlace(resJson);
      });
  }

  // get places
  async function getPlaces() {
    const url = config.API_URL + 'v1/places';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        const result = [];
        if (resJson.length >= 4) {
          for (var i = 0; i < 4; i++) {
            result.push(resJson[i]);
          }
          setPlaces(result);
        } else {
          for (var i = 0; i < resJson.length; i++) {
            result.push(resJson[i]);
          }
          setPlaces(result);
        }
      });
  }

  // get hotel by place id
  async function getHotels() {
    const url = config.API_URL + 'v1/hotels/tophotelsinplace/' + params;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setHotels(resJson);
      });
  }

  // add favorite place
  async function addFavoritePlace(placeId) {
    const url = config.API_URL + 'v1/favorite_places';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        userId: 'cd925b7d-5136-4cd6-8708-9023fc6f97cc',
        placeId: placeId,
      }),
    })
      .then(function (res) {
        console.log(res);
      })
      .catch(function (res) {
        console.log(res);
      });
  }

  const goToPost = props => {
    navigation.navigate({name: 'PlaceDetail', params: props});
  };

  const goBack = () => {
    navigation.goBack();
  };

  function goToHotel(hotelid, provinceid) {
    navigation.navigate({
      name: 'HotelDetails',
      params: {hotelid: hotelid, provinceid: provinceid},
    });
  }

  return (
    <View>
      <FlatList
        data={place}
        keyExtractor={item => item.placeid}
        renderItem={({item}) => {
          return (
            <View style={{backgroundColor: 'white', flex: 1}}>
              <ImageBackground
                source={item.photo}
                style={styles.image}
                imageStyle={{
                  borderBottomLeftRadius: 30,
                  borderBottomRightRadius: 30,
                }}>
                <Text style={styles.Tagline}>{item.namenative}</Text>
                <Text style={styles.Placename}>{item.name}</Text>
                <TouchableOpacity
                  onPress={goBack}
                  style={{
                    position: 'absolute',
                    left: 20,
                    top: 40,
                    backgroundColor: '#ff6200',
                    padding: 10,
                    borderRadius: 40,
                  }}>
                  <Feather name="arrow-left" size={22} color="#fff" />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => addFavoritePlace(item.placeid)}
                  style={{
                    position: 'absolute',
                    right: 20,
                    top: 40,
                    backgroundColor: '#ff6200',
                    padding: 10,
                    borderRadius: 40,
                  }}>
                  <Feather name="heart" size={22} color="#fff" />
                </TouchableOpacity>
              </ImageBackground>
              <ScrollView style={{backgroundColor: 'white'}}>
                <Text style={{padding: 14, fontSize: 20, fontWeight: 'bold'}}>
                  About the Place
                </Text>
                <Text
                  style={{
                    paddingHorizontal: 14,
                    fontSize: 14,
                    fontWeight: 'normal',
                    opacity: 0.3,
                    justifyContent: 'flex-start',
                    textAlign: 'justify',
                    lineHeight: 26,
                  }}>
                  {item.description}
                </Text>
                <View>
                  <View
                    style={{
                      padding: 14,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text style={{fontSize: 20, fontWeight: 'bold'}}>
                      Recommend Hotel
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#ff6200',
                      }}>
                      View All
                    </Text>
                  </View>
                  <View>
                    <FlatList
                      data={hotels}
                      keyExtractor={item => item.hotelid}
                      renderItem={({item}) => {
                        return (
                          <View style={{paddingVertical: 12, paddingLeft: 16}}>
                            <TouchableOpacity
                              onPress={() =>
                                goToHotel(item.hotelid, item.provinceid)
                              }>
                              <View
                                style={{
                                  backgroundColor: '#ff6200',
                                  width: '95%',
                                  borderRadius: 10,
                                  shadowColor: '#000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 5,
                                  },
                                  shadowOpacity: 0.5,
                                  shadowRadius: 6.68,
                                  elevation: 11,
                                }}>
                                <View>
                                  <Image
                                    source={item.photo}
                                    style={{
                                      width: 150,
                                      marginRight: 8,
                                      height: 150,
                                      borderRadius: 10,
                                    }}
                                  />
                                  <View style={styles.ImageOverlay}></View>
                                  <Feather
                                    name="map-pin"
                                    size={20}
                                    color="white"
                                    style={styles.imageLocationIcon}
                                  />
                                  <Text style={styles.ImageText}>
                                    {item.name}
                                  </Text>
                                </View>
                                <View>
                                  <Text style={styles.ImageTextDescription}>
                                    {item.description.substring(0, 230)}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                </View>
                <View>
                  <Text style={{padding: 14, fontSize: 20, fontWeight: 'bold'}}>
                    Suggested Places
                  </Text>
                  <FlatList
                    data={places}
                    horizontal={true}
                    keyExtractor={item => item.placeid}
                    renderItem={({item}) => {
                      return (
                        <View style={{paddingBottom: 30}}>
                          <TouchableOpacity
                            onPress={() => goToPost(item.placeid)}>
                            <View>
                              <Image
                                source={item.photo}
                                style={{
                                  width: 150,
                                  height: 150,
                                  marginHorizontal: 10,
                                  borderRadius: 10,
                                }}
                              />
                              <View style={styles.darkOverlay}></View>
                              <Feather
                                name="map-pin"
                                size={16}
                                color="white"
                                style={styles.imageLocationIcon}
                              />
                              <Text style={styles.ImageText}>{item.name}</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      );
                    }}
                  />
                </View>
                <View></View>
              </ScrollView>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  Tagline: {
    color: 'white',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginVertical: 6,
  },
  Placename: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginBottom: 30,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  BookTicketBtn: {
    position: 'absolute',
    right: 12,
    top: 275,
    backgroundColor: '#ff6200',
    padding: 16,
    borderRadius: 40,
    elevation: 5,
  },
  bookTicketText: {
    color: 'white',
    fontSize: 14,
  },
  ImageText: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 30,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  darkOverlay: {
    width: 150,
    height: 150,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  imageLocationIcon: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 10,
    bottom: 10,
  },
});

export default Post;
