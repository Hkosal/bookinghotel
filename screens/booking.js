import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import config from '../config';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';

const Booking = ({navigation}) => {
  useEffect(() => {
    getBooking();
  }, []);

  const [booking, setBooking] = useState();

  //get province places hotels
  async function getBooking() {
    const url = config.API_URL + 'v1/bookings/bookingbystatus';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setBooking(resJson);
      });
  }

  const goToHotel = props => {
    navigation.navigate({name: 'Payment', params: props});
  };

  return (
    <View>
      <View>
        <FlatList
          horizontal={true}
          data={booking}
          keyExtractor={item => item.key}
          renderItem={({item}) => {
            return (
              <View>
                <View style={styles.listPlace}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      marginRight: 1,
                    }}>
                    {item.name}
                  </Text>
                </View>
                <View>
                  <FlatList
                    data={item.booking}
                    keyExtractor={item => item.bookingid}
                    renderItem={({item}) => {
                      return (
                        <View
                          style={{
                            paddingVertical: 12,
                            paddingLeft: 16,
                            width: 400,
                          }}>
                          <View
                            style={{
                              backgroundColor: '#ff6200',
                              width: '95%',
                              borderRadius: 10,
                              shadowColor: '#000',
                              shadowOffset: {
                                width: 0,
                                height: 5,
                              },
                              shadowOpacity: 0.5,
                              shadowRadius: 6.68,
                              elevation: 11,
                            }}>
                            <View style={{margin: 15}}>
                              <Text style={{color: 'white'}}>
                                សណ្តាគារ: {item.namenative}
                              </Text>
                              <Text style={{color: 'white'}}>
                                Hotel: {item.name}
                              </Text>
                              <Text style={{color: 'white'}}>
                                សម្រាយ: {item.descriptionnative}
                              </Text>
                              <Text style={{color: 'white'}}>
                                Description: {item.description}
                              </Text>
                              <Text style={{color: 'white'}}>
                                Booking Date: {item.created}
                              </Text>
                            </View>
                          </View>
                        </View>
                      );
                    }}
                  />
                </View>
              </View>
            );
          }}
        />
        <View
          style={{
            padding: 14,
            flexDirection: 'row',
            justifyContent: 'space-between',
          }}></View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  listPlace: {
    padding: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  imageLocationIcon: {
    position: 'absolute',
    marginTop: 4,
    left: 10,
    bottom: 10,
  },
  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 30,
    bottom: 10,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 30,
    color: 'white',
    fontSize: 14,
  },
});

export default Booking;
