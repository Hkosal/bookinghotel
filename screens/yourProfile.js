/* eslint-disable react-native/no-inline-styles */
/* eslint-disable react/jsx-no-undef */
import React, {useState, useEffect} from 'react';
import {StyleSheet, View, Text, Image, FlatList} from 'react-native';
import config from '../config';

const Profile = ({navigation}) => {
  useEffect(() => {
    getProfile();
  }, []);

  const [user, setProfile] = useState();
  const user_id = 'cd925b7d-5136-4cd6-8708-9023fc6f97cc';
  //recently
  async function getProfile() {
    const url = config.API_URL + 'v1/users/user/' + user_id;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setProfile(resJson);
      });
  }

  return (
    <View>
      <FlatList
        data={user}
        keyExtractor={item => item.userid}
        renderItem={({item}) => {
          return (
            <View style={styles.userContent}>
              <View style={styles.imageContainer}>
                <Image
                  source={item.photo}
                  style={styles.image}
                  resizeMode="cover"
                />
              </View>
              <View>
                <Text style={styles.HeaderText}>
                  {item.firstname + ' ' + item.lastname}
                </Text>
                <Text style={styles.list}>Gender : {item.gender}</Text>
                <Text style={styles.list}>Phone : {item.phone}</Text>
                <Text style={styles.list}>E-mail : {item.email}</Text>
                <Text style={styles.list}>Address : {item.address}</Text>
              </View>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  userContent: {
    flex: 1,
    alignItems: 'center',
    paddingTop: 50,
  },
  imageContainer: {
    width: 200,
    height: 200,
    overflow: 'hidden',
    borderRadius: 200,
    backgroundColor: '#ff6200',
  },
  image: {
    width: '100%',
    height: '100%',
    borderRadius: 200,
  },
  HeaderText: {
    fontSize: 30,
    marginVertical: 5,
    textAlign: 'center',
    fontFamily: 'OpenSans-Regular',
  },
  list: {
    fontSize: 18,
    backgroundColor: 'white',
    padding: 5,
    paddingLeft: 10,
    margin: 5,
  },
});

export default Profile;
