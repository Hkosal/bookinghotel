import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
  ImageBackground,
  ScrollView,
} from 'react-native';
import config from '../config';
import {useRoute} from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';

const Booking = ({navigation}) => {
  const {params = []} = useRoute(); // get from hook

  useEffect(() => {
    getProvincePlacesHotels();
  }, []);

  const [provinces, setProvinces] = useState();

  //get province places hotels
  async function getProvincePlacesHotels() {
    const url =
      config.API_URL + 'v1/provinces/placeshotelsbyprovince/' + params;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setProvinces(resJson);
      });
  }

  const goToPost = props => {
    navigation.navigate({name: 'Post', params: props});
  };

  function goToHotel(hotelid, provinceid) {
    navigation.navigate({
      name: 'HotelDetails',
      params: {hotelid: hotelid, provinceid: provinceid},
    });
  }

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <View>
      <FlatList
        data={provinces}
        keyExtractor={item => item.key}
        renderItem={({item}) => {
          return (
            <View style={{backgroundColor: 'white', flex: 1}}>
              <ScrollView style={{backgroundColor: 'white'}}>
                <View>
                  <View>
                    <TouchableOpacity
                      onPress={goBack}
                      style={{
                        position: 'absolute',
                        left: 20,
                        backgroundColor: '#ff6200',
                        padding: 10,
                        borderRadius: 40,
                      }}>
                      <Feather name="arrow-left" size={22} color="#fff" />
                    </TouchableOpacity>
                  </View>
                  <View
                    style={{
                      padding: 14,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginLeft: 160,
                      }}>
                      Places
                    </Text>
                  </View>
                  <View>
                    <FlatList
                      data={item.places}
                      keyExtractor={item => item.placeid}
                      renderItem={({item}) => {
                        return (
                          <View style={{paddingVertical: 12, paddingLeft: 16}}>
                            <TouchableOpacity
                              onPress={() => goToPost(item.placeid)}>
                              <View
                                style={{
                                  backgroundColor: '#ff6200',
                                  width: '95%',
                                  borderRadius: 10,
                                  shadowColor: '#000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 5,
                                  },
                                  shadowOpacity: 0.5,
                                  shadowRadius: 6.68,
                                  elevation: 11,
                                }}>
                                <View>
                                  <Image
                                    source={item.photo}
                                    style={{
                                      width: 150,
                                      marginRight: 8,
                                      height: 150,
                                      borderRadius: 10,
                                    }}
                                  />
                                  <View style={styles.ImageOverlay}></View>
                                  <Feather
                                    name="map-pin"
                                    size={20}
                                    color="white"
                                    style={styles.imageLocationIcon}
                                  />
                                  <Text style={styles.ImageText}>
                                    {item.name}
                                  </Text>
                                </View>
                                <View>
                                  <Text style={styles.ImageTextDescription}>
                                    {item.description.substring(0, 230)}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                  <View
                    style={{
                      padding: 14,
                      flexDirection: 'row',
                      justifyContent: 'space-between',
                    }}>
                    <Text
                      style={{
                        fontSize: 20,
                        marginLeft: 160,
                        fontWeight: 'bold',
                      }}>
                      Hotels
                    </Text>
                  </View>
                  <View>
                    <FlatList
                      data={item.hotels}
                      keyExtractor={item => item.hotelid}
                      renderItem={({item}) => {
                        return (
                          <View style={{paddingVertical: 12, paddingLeft: 16}}>
                            <TouchableOpacity
                              onPress={() =>
                                goToHotel(item.hotelid, item.provinceid)
                              }>
                              <View
                                style={{
                                  backgroundColor: '#ff6200',
                                  width: '95%',
                                  borderRadius: 10,
                                  shadowColor: '#000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 5,
                                  },
                                  shadowOpacity: 0.5,
                                  shadowRadius: 6.68,
                                  elevation: 11,
                                }}>
                                <View>
                                  <Image
                                    source={item.photo}
                                    style={{
                                      width: 150,
                                      marginRight: 8,
                                      height: 150,
                                      borderRadius: 10,
                                    }}
                                  />
                                  <View style={styles.ImageOverlay}></View>
                                  <Feather
                                    name="map-pin"
                                    size={20}
                                    color="white"
                                    style={styles.imageLocationIcon}
                                  />
                                  <Text style={styles.ImageText}>
                                    {item.name}
                                  </Text>
                                </View>
                                <View>
                                  <Text style={styles.ImageTextDescription}>
                                    {item.description.substring(0, 230)}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                </View>
                <View></View>
              </ScrollView>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  Tagline: {
    color: 'white',
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginVertical: 6,
  },
  Placename: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginBottom: 30,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  BookTicketBtn: {
    position: 'absolute',
    right: 12,
    top: 275,
    backgroundColor: '#ff6200',
    padding: 16,
    borderRadius: 40,
    elevation: 5,
  },
  bookTicketText: {
    color: 'white',
    fontSize: 14,
  },
  ImageText: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 30,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  darkOverlay: {
    width: 150,
    height: 150,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  imageLocationIcon: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 10,
    bottom: 10,
  },
});

export default Booking;
