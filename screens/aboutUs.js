/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  ImageBackground,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';

const About = ({navigation}) => {
  const image = {
    uri: 'https://images.pexels.com/photos/227417/pexels-photo-227417.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  };

  const goBack = () => {
    navigation.goBack();
  };
  return (
    <View>
      <ImageBackground
        source={image}
        style={{width: '100%', height: 220}}
        imageStyle={{borderBottomRightRadius: 65}}>
        <View style={styles.DarkOverlay} />
        <View style={styles.searchContainer}>
          <Text style={styles.UserGreet}>Booking App</Text>
        </View>
        <TouchableOpacity
          onPress={goBack}
          style={{
            position: 'absolute',
            left: 20,
            top: 10,
            backgroundColor: '#ff6200',
            padding: 10,
            borderRadius: 40,
          }}>
          <Feather name="arrow-left" size={22} color="#fff" />
        </TouchableOpacity>
      </ImageBackground>
      <View style={styles.section}>
        <Text style={styles.main}>OUR VALUES </Text>
        <Text
          style={{
            fontSize: 16,
            padding: 10,
            textAlign: 'justify',
            lineHeight: 20,
          }}>
          Booking Hotel is online platform app that providing services for
          having trip(s) with many places and suitable price. Visitors can view
          many places and booking hotel as they prefer.
        </Text>
      </View>
      <View style={styles.section}>
        <Text style={styles.main}>OUR MISSION </Text>
        <Text
          style={{
            fontSize: 16,
            padding: 10,
            textAlign: 'justify',
            lineHeight: 20,
          }}>
          We want the project become the popular app in Cambodia and over the
          world in 5 Years. People Can sign up/sign in by using phone or email,
          can view all place in Cambodia and place over the world, can view
          place detail and history, can search place and hotel, can update the
          profile such ask name picture email phone and address, can booking
          hotel before their trip, have map to sell where the hotel or place.
          Finally, we to see user smile, and enjoy by using our app.{' '}
        </Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  DarkOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 220,
    backgroundColor: '#000',
    opacity: 0.2,
    borderBottomRightRadius: 65,
  },
  searchContainer: {
    paddingTop: 70,
    paddingLeft: 16,
  },
  UserGreet: {
    fontSize: 38,
    fontWeight: 'bold',
    color: 'white',
  },
  userText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'white',
  },
  searchBox: {
    marginTop: 16,
    backgroundColor: '#fff',
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: '90%',
  },
  ImageOverlay: {
    width: 380,
    height: 220,
    marginRight: 8,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
  },
  imageLocationIcon: {
    position: 'absolute',
    marginTop: 4,
    left: 10,
    bottom: 10,
  },
  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 30,
    bottom: 10,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 10,
    position: 'absolute',
    left: 75,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  main: {
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'bold',
  },
});

export default About;
