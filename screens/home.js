/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  FlatList,
  TextInput,
} from 'react-native';
import {SearchBar} from 'react-native-screens';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';
import config from '../config';

const Home = ({navigation}) => {
  useEffect(() => {
    getTopTreding();
    getRecently();
    getProvincePlacesHotels();
  }, []);
  const [topTreding, setTopTreding] = useState([]);
  const [recently, setResently] = useState();
  const [provinces, setProvinces] = useState();
  const [search, setSearch] = useState('');

  function onInputChange(value) {
    setSearch(value);
  }

  //get top treding
  async function getTopTreding() {
    const url = config.API_URL + 'v1/places/toptredings';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setTopTreding(resJson);
      });
  }
  //recently
  async function getRecently() {
    const url = config.API_URL + 'v1/places/recentlyplace';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setResently([resJson]);
      });
  }

  //get province places hotels
  async function getProvincePlacesHotels() {
    const url = config.API_URL + 'v1/provinces/provinceplaceshotels';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setProvinces(resJson);
      });
  }

  const image = {
    uri: 'https://images.pexels.com/photos/227417/pexels-photo-227417.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
  };

  const goToPost = props => {
    navigation.navigate({name: 'Post', params: props});
  };

  function goToHotel(hotelid, provinceid) {
    navigation.navigate({
      name: 'HotelDetails',
      params: {hotelid: hotelid, provinceid: provinceid},
    });
  }

  function searchBar() {
    navigation.navigate({name: 'Search', params: search});
  }

  return (
    <View style={{flexGrow: 1, height: '100%'}}>
      <View>
        <ImageBackground
          source={image}
          style={{width: '100%', height: 220}}
          imageStyle={{borderBottomRightRadius: 65}}>
          <View style={styles.DarkOverlay}></View>
          <View style={styles.searchContainer}>
            <Text style={styles.UserGreet}>Hi Kosal</Text>
            <Text style={styles.userText}>
              Where would you like to go today?
            </Text>
          </View>
          <View>
            <TextInput
              style={styles.searchBox}
              placeholder="Search Destination"
              onChangeText={onInputChange}
              placeholderTextColor="#666"></TextInput>
            <Icon
              onPress={() => searchBar()}
              name="search"
              size={22}
              color="#666"
              style={{position: 'absolute', top: 30, right: 60}}
            />
          </View>
        </ImageBackground>
      </View>

      <ScrollView>
        {/* Top Trending */}
        <View style={{padding: 20}}>
          {/* <Text>{topPlacesInProvince}</Text> */}
          <Text style={{fontSize: 22, fontWeight: 'bold'}}>Top Places</Text>
        </View>
        <View>
          <FlatList
            horizontal={true}
            data={topTreding}
            keyExtractor={item => item.placeid}
            renderItem={({item}) => {
              return (
                <View style={{paddingVertical: 12, paddingLeft: 16}}>
                  <TouchableOpacity onPress={() => goToPost(item.placeid)}>
                    <Image
                      source={item.photo}
                      style={{
                        width: 150,
                        marginRight: 8,
                        height: 220,
                        borderRadius: 10,
                      }}
                    />
                    <View style={styles.ImageOverlay}></View>
                    <Feather
                      name="map-pin"
                      size={20}
                      color="white"
                      style={styles.imageLocationIcon}
                    />
                    <Text style={styles.ImageText}>{item.name}</Text>
                  </TouchableOpacity>
                </View>
              );
            }}
          />
        </View>
        {/* Recently Place  */}
        <View style={{marginBottom: 60}}>
          <View
            style={{
              padding: 20,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <Text style={{fontSize: 22, fontWeight: 'bold'}}>Recently</Text>
          </View>
          <View>
            <FlatList
              horizontal={true}
              data={recently}
              keyExtractor={item => item.placeid}
              renderItem={({item}) => {
                return (
                  <View style={{paddingVertical: 12, paddingLeft: 16}}>
                    <TouchableOpacity onPress={() => goToPost(item.placeid)}>
                      <Image
                        source={item.photo}
                        style={{
                          width: 380,
                          height: 250,
                          borderRadius: 10,
                          alignSelf: 'center',
                        }}
                      />
                      <View style={styles.ImageOverlay}></View>
                      <View
                        style={{position: 'absolute', bottom: 0, padding: 16}}>
                        <View style={{flexDirection: 'row'}}>
                          <Feather
                            name="map-pin"
                            color="white"
                            size={22}
                            style={{
                              marginLeft: 10,
                              position: 'relative',
                              top: 4,
                            }}
                          />
                          <Text
                            style={{
                              fontSize: 22,
                              color: 'white',
                              fontWeight: 'normal',
                              marginBottom: 10,
                              marginHorizontal: 10,
                              textShadowOffset: {width: 2, height: 2},
                              textShadowRadius: 5,
                              textShadowColor: '#000',
                            }}>
                            {item.name}
                          </Text>
                        </View>
                        <Text
                          style={{
                            fontSize: 14,
                            color: 'white',
                            fontWeight: 'normal',
                            marginBottom: 4,
                            opacity: 0.9,
                            marginLeft: 16,
                            textShadowOffset: {width: 2, height: 2},
                            textShadowRadius: 10,
                            textShadowColor: '#000',
                          }}>
                          {item.description.substring(0, 200)}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </View>
        </View>
        {/* List Province with place and hotel */}
        <View>
          <FlatList
            horizontal={true}
            data={provinces}
            keyExtractor={item => item.provinceid}
            renderItem={({item}) => {
              return (
                <View>
                  <View style={styles.listPlace}>
                    <Text
                      style={{
                        fontSize: 20,
                        fontWeight: 'bold',
                        marginRight: 200,
                      }}>
                      {item.name}
                    </Text>
                    <Text
                      style={{
                        fontSize: 14,
                        fontWeight: 'bold',
                        color: '#ff6200',
                      }}>
                      View All
                    </Text>
                  </View>
                  <View
                    style={{
                      paddingLeft: 14,
                      opacity: 0.7,
                    }}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                      Places
                    </Text>
                  </View>
                  <View>
                    <FlatList
                      data={item.places}
                      keyExtractor={item => item.placeid}
                      renderItem={({item}) => {
                        return (
                          <View style={{paddingVertical: 12, paddingLeft: 16}}>
                            <TouchableOpacity
                              onPress={() => goToPost(item.placeid)}>
                              <View
                                style={{
                                  backgroundColor: '#ff6200',
                                  width: '95%',
                                  borderRadius: 10,
                                  shadowColor: '#000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 5,
                                  },
                                  shadowOpacity: 0.5,
                                  shadowRadius: 6.68,
                                  elevation: 11,
                                }}>
                                <View>
                                  <Image
                                    source={item.photo}
                                    style={{
                                      width: 150,
                                      marginRight: 8,
                                      height: 150,
                                      borderRadius: 10,
                                    }}
                                  />
                                  <Feather
                                    name="map-pin"
                                    size={20}
                                    color="white"
                                    style={styles.imageLocationIcon}
                                  />
                                  <Text style={styles.ImageText}>
                                    {item.name}
                                  </Text>
                                </View>
                                <View>
                                  <Text style={styles.ImageTextDescription}>
                                    {item.description.substring(0, 230)}
                                  </Text>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                  <View
                    style={{
                      paddingLeft: 14,
                      opacity: 0.7,
                    }}>
                    <Text style={{fontSize: 18, fontWeight: 'bold'}}>
                      Hotels
                    </Text>
                  </View>
                  <View>
                    <FlatList
                      data={item.hotels}
                      keyExtractor={item => item.hotelid}
                      renderItem={({item}) => {
                        return (
                          <View style={{paddingVertical: 12, paddingLeft: 16}}>
                            <TouchableOpacity
                              onPress={() =>
                                goToHotel(item.hotelid, item.provinceid)
                              }>
                              <View
                                style={{
                                  backgroundColor: '#ff6200',
                                  width: '95%',
                                  borderRadius: 10,
                                  shadowColor: '#000',
                                  shadowOffset: {
                                    width: 0,
                                    height: 5,
                                  },
                                  shadowOpacity: 0.5,
                                  shadowRadius: 6.68,
                                  elevation: 11,
                                }}>
                                <View>
                                  <Image
                                    source={item.photo}
                                    style={{
                                      height: 150,
                                      borderRadius: 10,
                                    }}
                                  />
                                  <View
                                    style={{
                                      position: 'absolute',
                                      bottom: 0,
                                    }}>
                                    <Text
                                      style={{
                                        fontSize: 22,
                                        color: 'white',
                                        fontWeight: 'normal',
                                        marginBottom: 10,
                                        marginHorizontal: 10,
                                      }}>
                                      {item.name}
                                    </Text>
                                    <View
                                      style={{
                                        flexDirection: 'row',
                                        alignItems: 'center',
                                      }}>
                                      <Feather
                                        name="map-pin"
                                        color="white"
                                        size={15}
                                        style={{
                                          marginLeft: 10,
                                          position: 'relative',
                                        }}
                                      />
                                      <Text
                                        // eslint-disable-next-line react-native/no-inline-styles
                                        style={{
                                          fontSize: 14,
                                          color: 'white',
                                          fontWeight: 'normal',
                                          marginBottom: 4,
                                          opacity: 0.9,
                                          marginLeft: 5,
                                          marginTop: 10,
                                        }}>
                                        {'village, commune, district, province'}
                                      </Text>
                                    </View>
                                  </View>
                                </View>
                              </View>
                            </TouchableOpacity>
                          </View>
                        );
                      }}
                    />
                  </View>
                </View>
              );
            }}
          />
          <View
            style={{
              padding: 14,
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}></View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  DarkOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 220,
    backgroundColor: '#000',
    opacity: 0.2,
    borderBottomRightRadius: 65,
  },
  searchContainer: {
    paddingTop: 70,
    paddingLeft: 16,
  },
  UserGreet: {
    fontSize: 38,
    fontWeight: 'bold',
    color: 'white',
  },
  userText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'white',
  },
  searchBox: {
    marginTop: 16,
    backgroundColor: '#fff',
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: '90%',
  },
  ImageOverlay: {
    width: 380,
    height: 220,
    marginRight: 8,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
  },
  imageLocationIcon: {
    position: 'absolute',
    marginTop: 4,
    left: 10,
    bottom: 10,
  },
  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 30,
    bottom: 10,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 10,
    position: 'absolute',
    left: 75,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  listPlace: {
    padding: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Home;
