/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import config from '../config';

const Favoriteplace = ({navigation}) => {
  useEffect(() => {
    getFavoritePlaces();
  }, []);

  const [places, setFavoritePlaces] = useState();
  const user_id = 'cd925b7d-5136-4cd6-8708-9023fc6f97cc';
  //recently
  async function getFavoritePlaces() {
    const url = config.API_URL + 'v1/favorite_places/' + user_id;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setFavoritePlaces(resJson);
        // setFavoritePlaces(JSON.stringify(resJson));
      });
  }

  const goToPost = props => {
    navigation.navigate({name: 'Post', params: props});
  };

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <View>
      {/* Recently Place  */}
      {/* <Text>{places}</Text> */}
      <ScrollView>
        <View style={{marginBottom: 60}}>
          <View
            style={{
              padding: 20,
              flexDirection: 'row',
              alignItems: 'center',
              justifyContent: 'space-between',
            }}>
            <TouchableOpacity
              onPress={goBack}
              style={{
                backgroundColor: '#ff6200',
                padding: 10,
                borderRadius: 40,
              }}>
              <Feather name="arrow-left" size={22} color="#fff" />
            </TouchableOpacity>
            <Text
              style={{
                fontSize: 22,
                fontWeight: 'bold',
                backgroundColor: '#ff6200',
                color: 'white',
                padding: 10,
                width: 200,
                textAlign: 'center',
                borderRadius: 5,
                borderTopEndRadius: 10,
              }}>
              Favorite Places
            </Text>
          </View>
          <View>
            <FlatList
              data={places}
              keyExtractor={item => item.favoriteplaceid}
              renderItem={({item}) => {
                return (
                  <View style={{paddingVertical: 12, paddingLeft: 16}}>
                    <TouchableOpacity onPress={() => goToPost(item.placeid)}>
                      <Image
                        source={item.photo}
                        style={{
                          width: 380,
                          height: 200,
                          borderRadius: 10,
                        }}
                      />
                      <View style={styles.ImageOverlay} />
                      <View
                        style={{position: 'absolute', bottom: 0, padding: 16}}>
                        <View style={{flexDirection: 'row'}}>
                          <Feather
                            name="map-pin"
                            color="white"
                            size={22}
                            style={{
                              marginLeft: 10,
                              position: 'relative',
                              top: 4,
                            }}
                          />
                          <Text
                            style={{
                              fontSize: 22,
                              color: 'white',
                              fontWeight: 'normal',
                              marginBottom: 10,
                              marginHorizontal: 10,
                            }}>
                            {item.name}
                          </Text>
                        </View>
                        <Text
                          // eslint-disable-next-line react-native/no-inline-styles
                          style={{
                            fontSize: 14,
                            color: 'white',
                            fontWeight: 'normal',
                            marginBottom: 4,
                            opacity: 0.9,
                            marginLeft: 16,
                          }}>
                          {item.description}
                        </Text>
                      </View>
                    </TouchableOpacity>
                  </View>
                );
              }}
            />
          </View>
        </View>
      </ScrollView>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  DarkOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 220,
    backgroundColor: '#000',
    opacity: 0.2,
    borderBottomRightRadius: 65,
  },
  searchContainer: {
    paddingTop: 70,
    paddingLeft: 16,
  },
  UserGreet: {
    fontSize: 38,
    fontWeight: 'bold',
    color: 'white',
  },
  userText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'white',
  },
  searchBox: {
    marginTop: 16,
    backgroundColor: '#fff',
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: '90%',
  },
  ImageOverlay: {
    width: 380,
    height: 220,
    marginRight: 8,
    borderRadius: 10,
    position: 'absolute',
    opacity: 0.2,
  },
  imageLocationIcon: {
    position: 'absolute',
    marginTop: 4,
    left: 10,
    bottom: 10,
  },
  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 30,
    bottom: 10,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 30,
    color: 'white',
    fontSize: 14,
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  listPlace: {
    padding: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default Favoriteplace;
