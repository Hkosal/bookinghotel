import React, {useState, useEffect, useRef} from 'react';
import {
  View,
  Text,
  Platform,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const Verify = ({navigation}) => {
  let textInput = useRef(null);
  const lengthInput = 6;
  const [internalVal, setInternalVal] = useState('');

  const onChangeText = val => {
    setInternalVal(val);
  };

  useEffect(() => {
    textInput.focus();
  }, []);

  const backToLogin = () => {
    navigation.navigate('Login');
  };
  const goToHome = () => {
    navigation.navigate('NavigationDrawer');
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="white" />
      <View style={styles.header}>
        <Text style={styles.text_header}>BookingApp.com</Text>
      </View>
      <View style={styles.form}>
        <Text style={styles.textForm}>Input your OTP code sent via SMS</Text>
        <View style={{alignItems: 'center'}}>
          <View style={styles.action}>
            <View style={styles.flex}>
              <TextInput
                ref={input => (textInput = input)}
                onChangeText={onChangeText}
                value={internalVal}
                maxLength={lengthInput}
                returnKeyType="none"
                keyboardType="numeric"
                style={{width: 0, height: 0}}
              />
              {/* <View style={styles.containerInput} /> */}
              {Array(lengthInput)
                .fill()
                .map((data, index) => (
                  <View style={styles.cellView}>
                    <Text
                      key={index}
                      style={
                        (styles.cellText,
                        {
                          borderBottomColor:
                            index === internalVal.length
                              ? '#FB6C6A'
                              : '#234DB7',
                        })
                      }
                      onPress={() => textInput.focus()}>
                      {internalVal && internalVal.length > 0
                        ? internalVal[index]
                        : ''}
                    </Text>
                  </View>
                ))}
            </View>
          </View>
        </View>
        <View style={styles.bottomView} />
        <View style={styles.flexButton}>
          <TouchableOpacity
            onPress={backToLogin}
            style={[styles.button, {backgroundColor: 'black'}]}>
            <Text style={styles.textButton}>Back</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={goToHome} style={styles.button}>
            <Text style={styles.textButton}>Verify</Text>
          </TouchableOpacity>
        </View>
      </View>
    </View>
  );
};

export default Verify;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ff6200',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 100,
  },
  form: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 32,
    textAlign: 'center',
  },
  textForm: {
    color: '#05375a',
    fontSize: 18,
    textAlign: 'center',
  },
  action: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  flex: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    color: '#05375a',
    display: 'none',
  },
  flexButton: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 50,
    padding: 10,
    borderRadius: 7,
  },
  button: {
    width: '40%',
    padding: 10,
    borderRadius: 7,
    backgroundColor: 'green',
  },
  textButton: {
    color: 'white',
    alignItems: 'center',
    textAlign: 'center',
    fontSize: 18,
    fontWeight: 'bold',
  },
  cellView: {
    paddingVertical: 11,
    width: 40,
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomWidth: 1.5,
  },
  bottomView: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-end',
    alignItems: 'center',
    marginBottom: 50,
  },
});
