import React from 'react';
import {
  View,
  Text,
  Platform,
  StyleSheet,
  StatusBar,
  TextInput,
  TouchableOpacity,
} from 'react-native';

const Login = ({navigation}) => {
  const goToVerify = () => {
    navigation.navigate('verify');
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="white" />
      <View style={styles.header}>
        <Text style={styles.text_header}>BookingApp.com</Text>
      </View>
      <View style={styles.form}>
        <Text style={styles.textForm}>Your Number</Text>
        <View style={styles.action}>
          <View style={styles.flex}>
            <Text>{'+855 | '}</Text>
            <TextInput
              placeholder="Phone NUmber"
              keyboardType="numeric"
              style={styles.TextInput}
            />
          </View>
        </View>
        <TouchableOpacity style={styles.button} onPress={goToVerify}>
          <Text style={[styles.textSign, {color: '#fff'}]}>Send</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ff6200',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 100,
  },
  form: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 32,
    textAlign: 'center',
  },
  textForm: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  flex: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
    backgroundColor: 'green',
    padding: 10,
    borderRadius: 7,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
