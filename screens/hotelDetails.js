/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  ImageBackground,
  ScrollView,
  TouchableOpacity,
  FlatList,
} from 'react-native';
import config from '../config';
import {useRoute} from '@react-navigation/native';
import Feather from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/FontAwesome';

const Hotel = ({navigation}) => {
  const {params = {}} = useRoute(); // get from hook

  useEffect(() => {
    getHotel();
    getPlaces();
  }, []);

  const [hotel, setHotel] = useState();
  const [places, setPlaces] = useState();

  const [modal] = useState(false);

  // get hotel
  async function getHotel() {
    const url = config.API_URL + 'v1/hotels/hotel/' + params.hotelid;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setHotel(resJson);
      });
  }

  // get hotel
  async function getPlaces() {
    const url =
      config.API_URL + 'v1/places/topplacesinprovince/' + params.provinceid;
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setPlaces(resJson);
      });
  }

  // add favorite hotel
  async function addFavoriteHotel(hotelid) {
    const url = config.API_URL + 'v1/favorite_hotels';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        userId: 'cd925b7d-5136-4cd6-8708-9023fc6f97cc',
        hotelId: hotelid,
      }),
    })
      .then(function (res) {
        console.log(res);
      })
      .catch(function (res) {
        console.log(res);
      });
  }
  // add favorite hotel
  async function addBooking(item) {
    const url = config.API_URL + 'v1/bookings';
    fetch(url, {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
      body: JSON.stringify({
        name: item.name,
        nameNative: item.namenative,
        description: item.description,
        descriptionNative: item.descriptionnative,
        hotelId: item.hotelid,
        photo: item.photo,
        userId: 'cd925b7d-5136-4cd6-8708-9023fc6f97cc',
        status: 'pedding',
      }),
    })
      .then(function (res) {
        console.log(res);
        alert('Your booking has been succeed');
      })
      .catch(function (res) {
        console.log(res);
      });
  }

  const goBack = () => {
    navigation.goBack();
  };

  const goToPost = props => {
    navigation.navigate({name: 'Post', params: props});
  };

  return (
    <View>
      <FlatList
        data={hotel}
        keyExtractor={item => item.hotelid}
        renderItem={({item}) => {
          return (
            <View style={{backgroundColor: 'white', flex: 1}}>
              <ImageBackground
                source={item.photo}
                style={styles.image}
                imageStyle={{
                  borderBottomLeftRadius: 30,
                  borderBottomRightRadius: 30,
                }}>
                <Text style={styles.Tagline}>{item.namenative}</Text>
                <Text style={styles.Placename}>{item.name}</Text>
                <TouchableOpacity
                  onPress={goBack}
                  style={{
                    position: 'absolute',
                    left: 20,
                    top: 40,
                    backgroundColor: '#ff6200',
                    padding: 10,
                    borderRadius: 40,
                  }}>
                  <Feather name="arrow-left" size={22} color="#fff" />
                </TouchableOpacity>
                <TouchableOpacity
                  onPress={() => addFavoriteHotel(item.hotelid)}
                  style={{
                    position: 'absolute',
                    right: 20,
                    top: 40,
                    backgroundColor: '#ff6200',
                    padding: 10,
                    borderRadius: 40,
                  }}>
                  <Feather name="heart" size={22} color="#fff" />
                </TouchableOpacity>
              </ImageBackground>
              <TouchableOpacity
                style={styles.BookTicketBtn}
                onPress={() => addBooking(item)}>
                <Text style={styles.bookTicketText}>Book Now</Text>
              </TouchableOpacity>
              <ScrollView style={{backgroundColor: 'white'}}>
                <Text style={{padding: 14, fontSize: 20, fontWeight: 'bold'}}>
                  About the Hotel
                </Text>
                <Text
                  style={{
                    paddingHorizontal: 14,
                    fontSize: 14,
                    fontWeight: 'normal',
                    opacity: 0.3,
                    justifyContent: 'flex-start',
                    textAlign: 'justify',
                    lineHeight: 26,
                  }}>
                  {item.description}
                </Text>
                <View>
                  <Text style={{padding: 14, fontSize: 20, fontWeight: 'bold'}}>
                    Suggested Places
                  </Text>
                  <FlatList
                    data={places}
                    horizontal={true}
                    keyExtractor={item => item.placeid}
                    renderItem={({item}) => {
                      return (
                        <View style={{paddingBottom: 30}}>
                          <TouchableOpacity
                            onPress={() => goToPost(item.placeid)}>
                            <View>
                              <Image
                                source={item.photo}
                                style={{
                                  width: 150,
                                  height: 150,
                                  marginHorizontal: 10,
                                  borderRadius: 10,
                                }}
                              />
                              <View style={styles.darkOverlay}></View>
                              <Feather
                                name="map-pin"
                                size={16}
                                color="white"
                                style={styles.imageLocationIcon}
                              />
                              <Text style={styles.ImageText}>{item.name}</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      );
                    }}
                  />
                </View>
                <View></View>
              </ScrollView>
            </View>
          );
        }}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  Tagline: {
    color: 'white',
    fontSize: 24,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginVertical: 6,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  Placename: {
    color: 'white',
    fontSize: 16,
    fontWeight: 'bold',
    paddingHorizontal: 14,
    marginBottom: 30,
    textShadowOffset: {width: 2, height: 2},
    textShadowRadius: 1,
    textShadowColor: '#000',
  },
  BookTicketBtn: {
    position: 'absolute',
    right: 12,
    top: 275,
    backgroundColor: '#ff6200',
    padding: 16,
    borderRadius: 40,
    elevation: 5,
  },
  bookTicketText: {
    color: 'white',
    fontSize: 14,
  },
  ImageText: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 30,
    bottom: 10,
    color: 'white',
    fontSize: 14,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 30,
    color: 'white',
    fontSize: 14,
  },
  darkOverlay: {
    width: 150,
    height: 150,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    borderRadius: 10,
    marginHorizontal: 10,
  },
  imageLocationIcon: {
    marginHorizontal: 14,
    marginTop: 4,
    position: 'absolute',
    left: 10,
    bottom: 10,
  },
});

export default Hotel;
