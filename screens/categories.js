/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  // Image,
  // ImageBackground,
  // ScrollView,
  // TextInput,
} from 'react-native';
import config from '../config';
// import Feather from 'react-native-vector-icons/Feather';
// import Icon from 'react-native-vector-icons/FontAwesome';

const Category = ({navigation}) => {
  useEffect(() => {
    getProvinces();
  }, []);

  const [provinces, setProvinces] = useState([]);

  //get provinces
  async function getProvinces() {
    const url = config.API_URL + 'v1/provinces';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setProvinces(resJson);
      });
  }

  const goToPost = props => {
    navigation.navigate({name: 'HotelsPlacesByProvince', params: props});
  };

  return (
    <View>
      <FlatList
        data={provinces}
        keyExtractor={item => item.provinceid}
        renderItem={({item}) => {
          return (
            <View
              style={{
                backgroundColor: '#ff6200',
                marginBottom: 5,
                paddingVertical: 5,
                paddingLeft: 16,
                paddingRight: 16,
                borderRadius: 10,
                shadowColor: '#000',
                shadowOffset: {
                  width: 0,
                  height: 5,
                },
                shadowOpacity: 0.2,
                shadowRadius: 6.68,
                elevation: 5,
              }}>
              <TouchableOpacity
                onPress={() => goToPost(item.provinceid)}
                style={{
                  padding: 5,
                  borderRadius: 10,
                }}>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  {item.namenative}
                </Text>
                <Text style={{color: 'white', fontWeight: 'bold'}}>
                  {item.name}
                </Text>
              </TouchableOpacity>
            </View>
          );
        }}
      />
    </View>
  );
};

// const styles = StyleSheet.create({});

export default Category;
