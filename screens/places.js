/* eslint-disable no-undef */
/* eslint-disable react-native/no-inline-styles */
import React, {useState, useEffect} from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  TouchableOpacity,
  FlatList,
} from 'react-native';

import Feather from 'react-native-vector-icons/Feather';
import config from '../config';

const place = ({navigation}) => {
  // eslint-disable-next-line react-hooks/rules-of-hooks
  useEffect(() => {
    getProvincePlacesHotels();
  }, []);

  // eslint-disable-next-line react-hooks/rules-of-hooks
  const [provinces, setProvinces] = useState();

  //get province places hotels
  async function getProvincePlacesHotels() {
    const url = config.API_URL + 'v1/provinces/provinceplaceshotels';
    fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        'Content-type': 'application/json',
      },
    })
      .then(res => res.json())
      .then(resJson => {
        setProvinces(resJson);
      });
  }

  const goToPost = props => {
    navigation.navigate({name: 'Post', params: props});
  };

  const goBack = () => {
    navigation.goBack();
  };

  return (
    <View>
      <TouchableOpacity
        onPress={goBack}
        style={{
          backgroundColor: '#ff6200',
          padding: 10,
          borderRadius: 40,
          width: 43,
          margin: 10,
        }}>
        <Feather name="arrow-left" size={22} color="#fff" />
      </TouchableOpacity>
      <View>
        <FlatList
          horizontal={true}
          data={provinces}
          keyExtractor={item => item.provinceid}
          renderItem={({item}) => {
            return (
              <View>
                <View style={styles.listPlace}>
                  <Text
                    style={{
                      fontSize: 20,
                      fontWeight: 'bold',
                      marginRight: 200,
                    }}>
                    {item.name}
                  </Text>
                  <Text
                    style={{
                      fontSize: 14,
                      fontWeight: 'bold',
                      color: '#ff6200',
                    }}>
                    View All
                  </Text>
                </View>
                <View
                  style={{
                    paddingLeft: 14,
                    opacity: 0.7,
                  }}>
                  <Text style={{fontSize: 18, fontWeight: 'bold'}}>Places</Text>
                </View>
                <View>
                  <FlatList
                    data={item.places}
                    keyExtractor={item => item.placeid}
                    renderItem={({item}) => {
                      return (
                        <View style={{paddingVertical: 12, paddingLeft: 16}}>
                          <TouchableOpacity
                            onPress={() => goToPost(item.placeid)}>
                            <View
                              style={{
                                backgroundColor: '#ff6200',
                                width: '95%',
                                borderRadius: 10,
                                shadowColor: '#000',
                                shadowOffset: {
                                  width: 0,
                                  height: 5,
                                },
                                shadowOpacity: 0.5,
                                shadowRadius: 6.68,
                                elevation: 11,
                              }}>
                              <View>
                                <Image
                                  source={item.photo}
                                  style={{
                                    height: 200,
                                    borderRadius: 10,
                                  }}
                                />
                                <View
                                  style={{
                                    position: 'absolute',
                                    bottom: 0,
                                  }}>
                                  <Text
                                    style={{
                                      fontSize: 22,
                                      color: 'white',
                                      fontWeight: 'normal',
                                      marginBottom: 10,
                                      marginHorizontal: 10,
                                    }}>
                                    {item.name}
                                  </Text>
                                  <View
                                    style={{
                                      flexDirection: 'row',
                                      alignItems: 'center',
                                    }}>
                                    <Feather
                                      name="map-pin"
                                      color="white"
                                      size={15}
                                      style={{
                                        marginLeft: 10,
                                        position: 'relative',
                                      }}
                                    />
                                    <Text
                                      // eslint-disable-next-line react-native/no-inline-styles
                                      style={{
                                        fontSize: 14,
                                        color: 'white',
                                        fontWeight: 'normal',
                                        marginBottom: 4,
                                        opacity: 0.9,
                                        marginLeft: 5,
                                        marginTop: 10,
                                      }}>
                                      {'village, commune, district, province'}
                                    </Text>
                                  </View>
                                </View>
                              </View>
                            </View>
                          </TouchableOpacity>
                        </View>
                      );
                    }}
                  />
                </View>
              </View>
            );
          }}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  DarkOverlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    height: 220,
    backgroundColor: '#000',
    opacity: 0.2,
    borderBottomRightRadius: 65,
  },
  searchContainer: {
    paddingTop: 70,
    paddingLeft: 16,
  },
  UserGreet: {
    fontSize: 38,
    fontWeight: 'bold',
    color: 'white',
  },
  userText: {
    fontSize: 16,
    fontWeight: 'normal',
    color: 'white',
  },
  searchBox: {
    marginTop: 16,
    backgroundColor: '#fff',
    paddingLeft: 24,
    padding: 12,
    borderTopRightRadius: 40,
    borderBottomRightRadius: 40,
    width: '90%',
  },
  ImageOverlay: {
    width: 380,
    height: 220,
    marginRight: 8,
    borderRadius: 10,
    position: 'absolute',
    backgroundColor: '#000',
    opacity: 0.2,
  },
  imageLocationIcon: {
    position: 'absolute',
    marginTop: 4,
    left: 10,
    bottom: 10,
  },
  ImageText: {
    position: 'absolute',
    color: 'white',
    marginTop: 4,
    fontSize: 14,
    left: 30,
    bottom: 10,
  },
  ImageTextDescription: {
    marginHorizontal: 80,
    justifyContent: 'flex-start',
    textAlign: 'justify',
    marginTop: 4,
    position: 'absolute',
    left: 75,
    bottom: 30,
    color: 'white',
    fontSize: 14,
  },
  image: {
    height: 300,
    justifyContent: 'flex-end',
  },
  listPlace: {
    padding: 14,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default place;
